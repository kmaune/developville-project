Developville - Social Coding
============================

This is a personal project of mine. Don't expect it to be completed anytime soon, I only work on it in my free time.

__What's Done__
- Multi-page layout
- Single-page layout

__What Needs To Be Done__
- Setup as Rails app
- Database
- Could use more styling
